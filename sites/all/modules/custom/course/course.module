<?php
/**
 * @file
 * Code for the Lacuna Stories Course feature.
 */

include_once 'course.features.inc';

define("COURSE_NODE_TYPE", 'course');
define("BIBLIO_NODE_TYPE", 'biblio');
define("DOCUMENT_NODE_TYPE", 'document');
define("COURSE_CURRENT_TABLE", 'course_current');
define("COURSE_CURRENT_CACHE_ID", 'course_current_cache');

/**
 * Implements hook_menu().
 */
function course_menu() {
  $items = array();
  $items['course-setup'] = array(
    'title' => 'Create a New Course',
    'description' => 'course creation.',
    'page callback' => 'course_create_authorize',
    'access callback' => 'course_access',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'course.pages.inc',
  );
  $items['course-setup/add-course'] = array(
    'title' => 'Creating a New Course',
    'description' => 'course creation.',
    'page callback' => 'course_create',
    'access callback' => 'course_access',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'course.pages.inc',
  );
  $items['course-setup/organize'] = array(
    'title' => 'Organize Materials',
    'description' => 'Organize course.',
    'page callback' => 'course_organize',
    'access callback' => 'course_access',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'course.pages.inc',
  );
  $items['course-setup/add-materials'] = array(
    'title' => 'Add Material',
    'description' => 'Add course material.',
    'page callback' => 'course_add_material',
    'access callback' => 'course_access',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'course.pages.inc',
  );
  $items['ajax/add-term/%'] = array(
    'page callback' => 'course_ajax_add_term',
    'page arguments' => array(2),
    'access callback' => 'course_access',
    'type' => MENU_CALLBACK,
    'file' => 'course.pages.inc',
  );
  $items['ajax/delete-term/%/%'] = array(
    'page callback' => 'course_ajax_delete_term',
    'page arguments' => array(2,3),
    'access callback' => 'course_access',
    'type' => MENU_CALLBACK,
    'file' => 'course.pages.inc',
  );
  $items['admin/config/content/lacuna-courses'] = array(
    'title' => 'Course Creation Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('course_admin_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
   );
  // Empty home (front) page, added onto via context
  $items['home'] = array(
    'page callback' => 'course_blank_page',
    'access callback' => TRUE,
    'type' => MENU_SUGGESTED_ITEM,
    'file' => 'course.pages.inc',
  );
  // "About This Course" callback. Link is defined separately in the feature.
  $items['about-course'] = array(
    'title' => 'About This Course',
    'page callback' => 'course_about_course',
    'access callback' => 'course_get_selected_course',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'course.pages.inc',
  );
  $items['course-tags/autocomplete/%/%'] = array(
    'page callback' => 'course_tags_autocomplete',
    'page arguments' => array(2, 3),
    'access callback' => 'course_get_selected_course',
    'type' => MENU_CALLBACK,
    'file' => 'course.pages.inc'
  );
  return $items;
}

/**
 * check permissions and token to create a course
 */
function course_access() {
  global $user;
  $key = "Course_Auth_Code_User_" . $user->uid;
  $canCreate = user_access('create course content');

  // we only want to check access when we are actually on a protected page
  // not just when eg the link is displayed and the access functions are called.
  if (!course_is_protected_page()) {
    return TRUE;
  }

  // No auth code required plus, permissions to create a course
  if ($canCreate && !variable_get('course_require_auth')) {
    return TRUE;
  }

  // Allow instructors to add materials to an existing course w/o course auth code
  $allowed_paths = array('course-setup/organize', 'course-setup/add-materials');
  $gid = course_get_selected_course();
  if ($canCreate && !empty($gid) && in_array(current_path(), $allowed_paths)) {
    return TRUE;
  }

  // ANON
  if (user_is_anonymous()) {
    drupal_set_message("You must be logged in to create a Lacuna Stories course.", 'status', FALSE);
    drupal_goto('user/login', array('query' => array('destination' => 'course-setup')));
  }
  // NO PERMS
  elseif (!$canCreate) {
    drupal_set_message("Please contact the Lacuna Stories Team. Your account does not have access to create a course.", 'error', FALSE);
    return FALSE;
  }
  elseif (($cache = cache_get($key)) && $_COOKIE['Drupal_visitor_' . $key] == $cache->data) {
    // VALID
    return TRUE;
  }
  // NO/MISMATCHED AUTHCODE
  else {
    drupal_set_message("Please type in the Authorization Code before creating a Lacuna Stories Course", 'warning', FALSE);
    drupal_goto('course-setup');
  }
}

function course_is_protected_page() {
  $current_path = current_path();
  return (preg_match("/course-setup/", $current_path) && $current_path != 'course-setup');
}

function course_admin_form() {
  $form = array();
  $form['authorization'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authorization'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['authorization']['course_require_auth'] = array(
    '#type' => 'radios',
    '#title' => t('Require Code to Create Courses?'),
    '#description' => t('Select "Yes" to require instructors to have the authorization code before creating a course.'),
    '#default_value' => variable_get('course_require_auth', TRUE),
    '#options' => array(TRUE => 'Yes', FALSE => 'No'),
    '#required' => TRUE,
  );

  $form['authorization']['course_authcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorization Code'),
    '#description' => t('Set the authorization code needed to create a Lacuna Stories Course.'),
    '#default_value' => variable_get('course_authcode', 'ChangeMe!'),
    '#required' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="course_require_auth"]' => array('value' => 1)
      )
    )
  );

  $form['taxonomies'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Taxonomies'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t('Set the default values that all new courses will receive. Separate each item by commas. Instructors will have a chance to change these when setting up their course.')
  );

  $form['taxonomies']['course_default_units'] = array(
    '#type' => 'textfield',
    '#title' => t('Units'),
    '#description' => t('Used to organize readings into units'),
    '#required' => TRUE,
    '#default_value' => variable_get('course_default_units', 'Week 1, Week 2'),
  );

  $form['taxonomies']['course_default_genre_doc'] = array(
    '#type' => 'textfield',
    '#title' => t('Genre - Materials'),
    '#description' => t('Used to classify materials by genre'),
    '#required' => TRUE,
    '#default_value' => variable_get('course_default_genre_doc', 'Fiction, Scholarship'),
  );

  $form['taxonomies']['course_default_genre_response'] = array(
    '#type' => 'textfield',
    '#title' => t('Genre - Responses'),
    '#description' => t('Used to classify student responses by genre'),
    '#required' => TRUE,
    '#default_value' => variable_get('course_default_genre_response', 'Essay, Fiction, Other, Reflection, Summary'),
  );

  return system_settings_form($form);
}

// Can user switch to given course?
function course_switch_access($gid) {
  return user_access('administer group') || og_is_member('node', $gid);
}

// Switch course context when a new course viewed, if allowed
function course_node_view($node, $view_mode, $langcode) {
  if (($node->type == 'course') && course_switch_access($node->nid)) {
    _course_set_selected_course($node->nid);
  }
}

/**
 * Implements hook_views_query_alter().
 * Updates existing views to that their results are limited to the current course
 */
function course_views_query_alter(&$view, &$query) {
	$gid = course_get_selected_course();
	if ($gid) {
		// Filter node content by course
		$where_node_views = array('responses', 'documents', 'threads', 'all_content', 'my_writing', 'sewing-kit', 'user_s_responses', 'user_s_comments', 'my_annotations_view');
		if (in_array($view->name, $where_node_views)) {
			$query->add_where_expression(0, "node.nid IN (SELECT etid FROM og_membership WHERE gid = " . $gid . " AND og_membership.entity_type = 'node')");
		}

		// Filter users by course
		$where_user_views = array('people', 'all_emails');
		if (in_array($view->name, $where_user_views)) {
			$query->add_where_expression(2, "users.uid IN (SELECT etid FROM og_membership WHERE gid = " . $gid . " AND og_membership.entity_type = 'user')");
		}

		// Filter annotation tags by course
		if ($view->name == 'curate_tags') {
      $taxonomy = course_get_terms_for_current_course('annotation_tags');
			$query->add_where(0, 'tid', array_keys($taxonomy), 'IN');
		}

    // Autocomplete Bibliography field in Responses with only relevant bibliographic entries
		if ($view->name == 'bibliography_entity_reference_autocomplete') {
      $result = db_query('SELECT b.field_bibliographic_entry_target_id
        FROM field_data_field_bibliographic_entry b
        LEFT JOIN node n ON n.nid = b.entity_id
        LEFT JOIN og_membership ogm ON n.nid = ogm.etid
        WHERE ogm.entity_type = :entity_type
        AND ogm.gid = :gid', array(':entity_type' => 'node', ':gid' => $gid));
      $biblio_ids = array_keys($result->fetchAllKeyed());
			$query->add_where(2, 'node.nid', $biblio_ids, 'IN');
		}
	}

	// Filter courses views for enrolled and not enrolled
	if ($view->name == 'courses') {
		switch ($view->current_display) {
			case 'not_enrolled':
				// This cannot be done in the view. It needs to be done in a query alter.
				// @see https://www.drupal.org/node/596860#comment-10030507
				$query->add_where_expression(2, 'node.nid NOT IN (SELECT gid FROM og_membership WHERE etid = ***CURRENT_USER***)');
				break;
			case 'enrolled':
				// This could be done in the view. Doing here for simplicity and organization.
				$query->add_where_expression(2, 'node.nid IN (SELECT gid FROM og_membership WHERE etid = ***CURRENT_USER***)');
				break;
		}
	}
}

/**
 * Implements hook_views_pre_render().
 */
function course_views_pre_render(&$view) {

  // User-contributed JS that prevents autosubmit on text views exposed text field filters for (while typing, still autosubmits on tab or enter). See https://www.drupal.org/node/1217948#comment-9233357
  // TODO: For now I have this loading on all views, but we may wish to only load for the specific views where this is needed
  // TODO: This is not not specific to courses. Once we have a general core feature, move this there
  drupal_add_js(drupal_get_path('module', 'course') . '/js/prevent-auto-submit.js', array('weight' => 10));
  switch ($view->name) {
    case 'courses':
      // "Not Enrolled" title is redundant when anon is viewing (front page), so hide the title
      if (user_is_anonymous()) {
        $view->set_title(' '); // space required
      }
      break;
    case 'archived_materials':
      drupal_add_css(drupal_get_path('module', 'course') . '/course.css');
      break;
  }
}

/*
 * Implements hook_views_pre_build().
 */
function course_views_pre_build(&$view) {
  switch($view->name) {

    case 'sewing-kit':

      // Set some default values on the exposed filters for Course and Annotator
      // There is a bug that prevents clearing default values (ie. searching for all) when the exposed filter is multivalued (see: https://www.drupal.org/node/2452729), so for now I have set these two filters to single value.
//      $current_course = course_get_selected_course();
//      $view->display_handler->handlers['filter']['og_group_ref_target_id']->value = array($current_course=>$current_course);
//      global $user;
//			if (isset($view->display_handler->handlers['filter']['author_select'])) {
//				$view->display_handler->handlers['filter']['author_select']->value = array($user->uid => $user->uid);
//			}


      break;

  }
}

// Filter taxonomy terms to return only those relevant for current course
function course_get_terms_for_current_course($taxonomy_name) {
	$gid = course_get_selected_course();
	$taxonomy = array();
	if ($gid) {
		$query = new EntityFieldQuery();
		$result = $query->entityCondition('entity_type', 'taxonomy_term')
			->entityCondition('bundle', $taxonomy_name)
			->fieldCondition('field_term_course', 'target_id', $gid, '=')
			->propertyOrderBy('weight')
			->execute();
		if (isset($result['taxonomy_term'])) {
			foreach(array_keys($result['taxonomy_term']) as $tid) {
				$wrapper = entity_metadata_wrapper('taxonomy_term', taxonomy_term_load($tid));
				$taxonomy[$tid] = $wrapper->name->value();
			}
		}
	}
	return $taxonomy;
}

// Returns list of documents in Ready to Annotate status for current course
function course_get_documents_for_current_course() {
	$gid = course_get_selected_course();
	$query = new EntityFieldQuery();
	$result = $query->entityCondition('entity_type', 'node')
		->entityCondition('bundle', 'document')
		->fieldCondition('og_group_ref', 'target_id', $gid)
		->fieldCondition('field_status', 'value', WORKFLOW_READY_STATE)
		->execute();
	if (isset($result['node'])) {
		return $result['node'];
	}
}

/*
 * Filter the bibliography nodes to those used in current course
 */
function course_get_bibliography_for_current_course() {
	$documents = course_get_documents_for_current_course();
	$biblios = array();
	if (!empty($documents)) {
		$docs = node_load_multiple(array_keys($documents));
		foreach ($docs as $doc) {
			$wrapper = entity_metadata_wrapper('node', $doc);
			array_push($biblios, $wrapper->field_bibliographic_entry->value());
		}
	}
	return $biblios;
}

/*
 * Get a list of relevant authors for exposed filter
 */
function course_get_authors_for_current_course() {
	$authors = array();
	$biblios = course_get_bibliography_for_current_course();
	foreach ($biblios as $biblio) {
		foreach ($biblio->biblio_contributors as $author) {
				$authors[$author['lastname']] = $author['name'];
		}
	}
	return $authors;
}

// Limit an exposed filter for taxonomy terms to those given
function course_limit_exposed_filter_tags(&$form, $field, $terms) {
  foreach ($form[$field]['#options'] as $tid => $term) {
    if (!isset($terms[$tid])) {
      unset($form[$field]['#options'][$tid]);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function course_form_views_exposed_form_alter(&$form, &$form_state) {
  if ('views-exposed-form-documents-materials' == $form['#id']) {
    // Set exposed filter 'Limit by Unit' for current course
		$form['field_unit_tid']['#options'] = course_get_terms_for_current_course('units');
		// Field name got reversed in Document field. Oops!
		// TODO: Fix the name
		$form['field_doc_genre_tid']['#options'] = course_get_terms_for_current_course('genre_doc');
		$form['lastname']['#options'] = course_get_authors_for_current_course();
	}

  if ('views-exposed-form-responses-page' == $form['#id']) {
    // Limit response genre per course
    // TODO: Fix response_genre -> genre_response order swap
    $form['field_response_genre_tid']['#options'] = course_get_terms_for_current_course('genre_response');
  }

	// Use display name via Realname module, not user account name for author exposed form
	// Filter authors to only those in current course
	if (isset($form['author_select'])) {
		$uids = array_keys($form['author_select']['#options']);
		$form['author_select']['#options'] = array();	// wipe options
		$gid = course_get_selected_course();
		foreach ($uids as $uid) {
			$account = user_load($uid);
			$groups = og_get_groups_by_user($account, 'node');
			if (!empty($groups) && in_array($gid, $groups)) {
				$form['author_select']['#options'][$uid] = format_username($account);
			}
		}
	}

  // Filter Taxonomy terms
  if (isset($form['field_tags_tid'])) {
    course_limit_exposed_filter_tags($form, 'field_tags_tid', course_get_terms_for_current_course('annotation_tags'));
//    $form['field_tags_tid']['#options'] = course_get_terms_for_current_course('annotation_tags');
  }

  if ($form['#id'] == 'views-exposed-form-sewing-kit-page') {
    // Restrict the tag filter options to tags on annotations on documents in courses in which the user is a member. Otherwise we will have inapplicable tags here even with no filters set. Plus tags may be considered private content.
    /* NOTE: To make Sewing Kit work *across* courses for users, uncomment this section

// Restrict the group filter options to (a) courses, (b) that the user is a member of
    $group_options = array('All' => '- Any -');
    $groups = og_get_groups_by_user();
    foreach ($groups['node'] as $gid) {
      $group = node_load($gid);
      if ($group->type == 'course') {
        $group_options[$gid] = $group->title;
      }
    }
    $form['og_group_ref_target_id']['#options'] = $group_options;


    $group_tag_options = array();
    $user_groups = array_keys(og_get_groups_by_user(NULL, 'node'));
    $course_groups = array();
    foreach ($user_groups as $user_group) {
      $group = node_load($user_group);
      if ($group->type == 'course') {
        $course_groups[] = $user_group;
      }
    }
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'taxonomy_term');
    $query->entityCondition('bundle', 'annotation_tags');
    // For trans-course Sewing Kit
    $query->fieldCondition('field_term_course', 'target_id', $course_groups, 'IN');
    $records = $query->execute();
    if (count($records)) {
      $group_tag_options = array_keys($records['taxonomy_term']);
    }
    foreach ($form['field_annotation_tags_tid']['#options'] as $existing_tag_option_tid => $existing_tag_option_name) {
      if (!in_array($existing_tag_option_tid, $group_tag_options)) {
        unset ($form['field_annotation_tags_tid']['#options'][$existing_tag_option_tid]);
      }
    }*/

    // For trans-course Sewing Kit, comment out the next line
    course_limit_exposed_filter_tags($form, 'field_annotation_tags_tid', course_get_terms_for_current_course('annotation_tags'));

    // Restrict exposed filter list of annotated content to current course
    $gid = course_get_selected_course();
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', array('document', 'response'), 'IN')
      ->fieldCondition('og_group_ref', 'target_id', $gid)
      ->execute();
    if (isset($result['node'])) {
      $nids = array_keys($result['node']);
    }
    foreach ($form['document']['#options'] as $type => $values) {
      foreach ($form['document']['#options'][$type] as $nid => $title) {
        if (!in_array($nid, $nids)) {
          unset($form['document']['#options'][$type][$nid]);
        }
      }
    }
  }
}

// Shared With field in the Sewing Kit view, merges the Audience field and Peer Groups values (when applicable) into a single column to save space
function course_sewing_kit_shared_with($data) {
  $lines = array();
  foreach ($data->field_field_annotation_audience as $audience) {
    $audience_val = $audience['rendered']['#markup'];
    if ($audience['raw']['value'] == 'peer-groups') {
      $peer_groups = array();
      foreach ($data->field_og_group_ref as $group) {
        if ($group['raw']['entity']->type == 'peer_group') {
          $peer_groups[] = $group['rendered']['#markup'];
        }
      }
      $audience_val .= ' (' . implode (', ', $peer_groups) . ')';
    }
    $lines[] = $audience_val;
  }
  $output = implode (', ', $lines);
  if ($output) {
    return $output;
  }
}

// Get the currently selected course
// Used extensively, so be careful if you refactor
function course_get_selected_course() {
  global $user;
	$result = db_query('SELECT c.gid FROM {' . COURSE_CURRENT_TABLE . '} c WHERE c.uid = :uid', array(':uid' => (int) $user->uid));
	foreach ($result as $record) {
		return($record->gid);
	}
}

function _course_set_selected_course($gid) {
  // NB: do your own error handling and permissions checking if you use this setter directly
  global $user;
  // Global user does not contain OG memberships, so saving that will lose them for the user. Load the user first and use that
  // see: https://www.drupal.org/node/1502916#comment-6444954.
	if (empty($gid)) {
		return;
	}
	try {
		db_merge(COURSE_CURRENT_TABLE)
			->key(array('uid' => (int) $user->uid))
			->fields(array('gid' => $gid))
			->execute();
	}
	catch (Exception $e) {
		watchdog_exception('_course_set_selected_course', $e, NULL, NULL, WATCHDOG_ERROR);
		drupal_set_message('Sorry, but an error occurred. Please notify a site administrator.', 'error');
		drupal_goto(drupal_get_destination());
	}
}

/*
 * Implements hook_node_validate
 */
function course_node_validate($node, $form, &$form_state) {
  if ($node->type == 'course') {
    // Ensure that multi-term courses are in consecutive terms
    $terms = $form_state['values']['field_course_term'][$node->language];
    if (count($terms) == 2) {
      $term1 = $terms[0]['value'];
      $term2 = $terms[1]['value'];
      if (($term1 == 'Fall' && $term2 == 'Spring') OR ($term1 = 'Winter' && $term2 == 'Summer')) {
        form_set_error('field_course_term', t('Multi-term courses must be in consecutive terms.'));
        // Todo: get this to highlight the term field in the failed form submission. Not sure what's up.
      }
    }
  }
}

function course_node_update($node) {
  global $user;
  $key = "Course_Clone_Material_Docs_" . $user->uid;
  // If any the material was cloned, we needed the user to finish all the course-specific bits, check if they are saving
  // one of those nodes and remove that node from the to finish queue cookie
  if (DOCUMENT_NODE_TYPE == $node->type && isset($_COOKIE['Drupal_visitor_' . $key]) && $docs = $_COOKIE['Drupal_visitor_' . $key]) {
    course_pop_unfinished_doc_queue($node->nid);
  }

}

function course_form_document_node_form_alter(&$form, &$form_state, $form_id) {
  $document = node_load($form['nid']['#value']);
	$unit_options = course_get_terms_for_current_course('units');
	$genre_options = course_get_terms_for_current_course('genre_doc');
  $units = array_keys($unit_options);
	$default_unit = array_shift($units);
  $genres = array_keys($genre_options);
	$default_genre = array_shift($genres);

	// Per-course units taxonomy
	if (isset($document->field_unit[LANGUAGE_NONE])) {
		$default_unit = $document->field_unit[LANGUAGE_NONE][0]['tid'];
	}
  $form['units'] = array(
    '#title' => 'Units',
    '#type' => 'select',
    '#size' => 60,
    '#default_value' => $default_unit,
    '#options' => $unit_options,
		'#required' => TRUE,
  );

	// Per-course Materials Genre taxonomy
	if (isset($document->field_doc_genre[LANGUAGE_NONE])) {
		$default_genre = $document->field_doc_genre[LANGUAGE_NONE][0]['tid'];
	}
	$form['genre_doc'] = array(
		'#title' => 'Genres',
		'#type' => 'select',
		'#size' => 60,
		'#default_value' => $default_genre,
		'#options' => $genre_options,
		'#required' => TRUE,
	);

	// Add our filtered fields to the correct groups
  $form['#group_children']['units'] = 'group_syllabus';
  $form['#group_children']['genre_doc'] = 'group_about';

	// Hide several default fields from user input - we're overriding them above
  $form['field_copyright']['#access'] = FALSE;
  $form['field_tags']['#access'] = FALSE;
  $form['field_unit']['#access'] = FALSE;
	$form['field_doc_genre']['#access'] = FALSE;
  $form['additional_settings']['#access'] = FALSE;
  $form['og_group_ref']['#access'] = FALSE;
  $form['field_bibliographic_entry']['#access'] = FALSE;

	// Finish up form
  $form['title']['#title'] = 'Title to Be Displayed to Students';
  $form['actions']['submit']['#value'] = 'Save (Finished Adding Material)';
  $form['actions']['submit']['#submit'] = array('course_add_document_form_submit');

  $form['actions']['submit2'] = array(
    '#type' => 'submit',
    '#submit' => array('course_add_document_form_submit'),
    '#value' => 'Save (Add More Material)',
  );

	$path = drupal_get_path('module', 'course');
  $form['#attached']['css'] = array("$path/course.css");
}

// Filter fields in Responses to current course context
function course_form_response_node_form_alter(&$form, &$form_state, $form_id) {
	$response = node_load($form['nid']['#value']);
	// Filter Response Genre to course
	$genre_options = course_get_terms_for_current_course('genre_response');
  $tids = array_keys($genre_options); // Avoid PHP warning about passing by reference :(
	$default_genre = array_shift($tids);
	if (isset($response->field_response_genre[LANGUAGE_NONE])) {
		$default_genre = $response->field_response_genre[LANGUAGE_NONE][0]['tid'];
	}
	$form['genre_response'] = array(
		'#title' => t('Genres'),
		'#type' => 'select',
		'#size' => 60,
		'#default_value' => $default_genre,
		'#options' => $genre_options,
		'#required' => TRUE,
	);
	$form['#group_children']['genre_response'] = 'group_genre_about';
	$form['field_response_genre']['#access'] = FALSE;	// hide this field

  // Customize the autocomplete to filter by course tags
  // Note: hard-coded taxonomy name
  // would be better to load this dynamically based on the existing field's taxonomy reference
  $lang = $form['language']['#value'];
  $form['field_tags'][$lang]['#autocomplete_path'] = 'course-tags/autocomplete/annotation_tags';

  $form['og_group_ref']['#access'] = FALSE;	// We'll set the course automatically
  $form['group_content_access']['#access'] = FALSE; // hide confusing option; always private
  $form['actions']['submit']['#submit'] = array('course_add_response_form_submit');
	$form['actions']['submit']['#value'] = 'Save Response';
}

// Handle setting custom values
function course_add_response_form_submit($form, &$form_state) {

  $node = node_form_submit_build_node($form, $form_state);
	$node->status = NODE_PUBLISHED;

	// Update node with our filtered values
  $wrapper = entity_metadata_wrapper('node', $node);
  $wrapper->field_response_genre[] = $form_state['values']['genre_response'];
  $wrapper->group_content_access->set(OG_CONTENT_ACCESS_PRIVATE);
  $wrapper->save();
  node_save($node);

  // Automatically assign to current course
  $gid = course_get_selected_course();
  if ($gid && $node->nid) {
    og_group('node', $gid,
      array(
        'entity_type' => 'node',
        'entity' => $node,
        'field_name' => 'og_group_ref',
        'state' => OG_STATE_ACTIVE
      )
    );
    drupal_goto(drupal_get_path_alias('node/' . $node->nid));
  } else {
    drupal_set_message('There was a problem saving your response', 'warning');
  }
}

function course_form_biblio_node_form_alter(&$form, &$form_state, $form_id) {
  $form['copyright_status'] = array(
    '#type' => 'hidden',
    '#default_value' => -1,
  );
  $form['#submit'] = array('course_add_biblio_form_submit');
  $form['actions']['submit']['#submit'][] = 'course_add_biblio_form_submit';
  $form['actions']['submit']['#value'] = 'Save Bibliographic Information';
  $form['additional_settings']['#access'] = FALSE;

  unset($form['actions']['preview']);
  $path = drupal_get_path('module', 'course');
  $form['#attached']['css'] = array("$path/course.css");
  $form['#attached']['js'] = array("$path/course.js");
}

// NB: Needs to be in .module so it is always loaded or it can be excluded from module_implements() cache
function course_form_course_node_form_alter(&$form, &$form_state, $form_id) {
	// ensure AJAX media upload still has all the includes
	// @see https://www.drupal.org/node/1118114#comment-4575298
	$files = (isset($form_state['build_info']['files'])) ? $form_state['build_info']['files'] : array();
	$files[] = drupal_get_path('module', 'node') . '/node.pages.inc';
	$form_state['build_info']['files'] = $files;
  $form['#submit'] = array('course_create_form_submit');
  $form['actions']['submit']['#submit'][] = 'course_create_form_submit';
}

function course_create_form_submit($form, &$form_state) {
	global $user;
	// Only handle new courses, because we create these during the course setup process
	if (empty($form['nid']['#value'])) {
		$values = array(
			'type' => COURSE_NODE_TYPE,
			'uid' => $user->uid,
			'status' => NODE_PUBLISHED,
			'comment' => 0,
			'promote' => 0,
		);
		$course = entity_create('node', array_merge($form_state['values'], $values));
		$course_wrapper = entity_metadata_wrapper('node', $course);
		try {
			$course_wrapper->save();
			$nid = $course_wrapper->nid->value();
			if (!empty($nid)) {
				_course_set_selected_course($nid);
				drupal_goto("course-setup/organize");
			}
		}
		catch (Exception $e) {
			watchdog_exception('course_create_form_submit', $e, NULL, NULL, WATCHDOG_ERROR);
			drupal_set_message("There was an error creating your course.  Please try again and/or contact the Lacuna Stories team for assistance, <a href='info@lacunastories.com'>info@lacunastories.com</a>.", 'error');
			drupal_goto("course-setup/add-course");
		}
	}
}

// Create a new, blank document to associate with the new biblio node
// Pre-populated with bibliographic entity reference
function course_create_blank_document($biblio) {
  $gid = course_get_selected_course();

  // have to save in a cookie b/c biblio module form refreshes the page on type selection and borks with initial form render
  $copyright = '';
  if (isset($_COOKIE['course-material-type-' . $gid])) {
    $copyright = check_plain($_COOKIE['course-material-type-' . $gid]);
  }

  $document = new stdClass();
  $document->type = DOCUMENT_NODE_TYPE;
  $document->language = LANGUAGE_NONE;
  $document->title = $biblio->title;
  $document->field_doc_text[LANGUAGE_NONE][0]['value'] = $biblio->body[LANGUAGE_NONE][0]['value'];
  $document->field_doc_text[LANGUAGE_NONE][0]['format']  = filter_default_format();

  node_object_prepare($document);

  $document->field_bibliographic_entry[LANGUAGE_NONE][0]['target_id'] = $biblio->nid;
  $document->status = NODE_PUBLISHED; // make all documents published
  $copyright_terms = taxonomy_get_term_by_name($copyright);
  $taxo = array_pop($copyright_terms);
  if ($taxo) {
    $document->field_copyright[LANGUAGE_NONE][0]['tid'] = $taxo->tid;
  }
  node_save($document);

  course_push_unfinished_doc_queue($document->nid);

  // Add to course group
  og_group('node', $gid, array(
    'entity_type' => 'node',
    'entity' => $document,
    'field_name' => 'og_group_ref',
    'state' => OG_STATE_ACTIVE
  ));
  $form_state['rebuild'] = TRUE;

  if ($document->nid) {
    drupal_set_message("The document's bibliographic information has been saved. Now provide the document's text and course-specific information.");
    drupal_goto("node/" . $document->nid . "/edit");
  } else {
    drupal_set_message("There was an error adding your material. Please try again or contact the Lacuna Stories team.", 'error');
  }
}

function course_add_biblio_form_submit($form, &$form_state) {
  $biblio = new stdClass();
  $biblio->type = BIBLIO_NODE_TYPE;
  foreach($form_state['values'] as $k => $v) {
    $biblio->$k = $v;
  }
  node_object_prepare($biblio);
  $biblio->status = NODE_PUBLISHED; // make all biblios published

  $isUpdate = isset($biblio->nid) || isset($biblio->is_new);
  node_save($biblio);
  // If this is an update to an existing biblio node,
  // Then do not add to unfinished queue
  // And do not create a new blank document
  if (!$isUpdate) {
    course_create_blank_document($biblio);
  }
}

function course_add_document_form_submit($form, &$form_state) {
	global $base_url;

	$node = node_form_submit_build_node($form, $form_state);
  $node->status = NODE_PUBLISHED;

	// Update document node with our filtered values
  $node->field_unit[LANGUAGE_NONE][0]['tid'] = $form_state['values']['units'];
	// TODO: Rename field_doc_genre to field_genre_doc
	$node->field_doc_genre[LANGUAGE_NONE][0]['tid'] = $form_state['values']['genre_doc'];
  node_save($node);

	$message = "You have successfully added material to your course. To make the item available for annotation, remember to change the status to 'Ready for Annotation'. You can access all your course materials under <a href=\"" . $base_url . "/all-content\">Manage->All Content</a>";

  if ("Save (Finished Adding Material)" == $form_state['values']['op']) {
    drupal_set_message($message);
    unset($_GET['destination']);
    drupal_goto("/materials");
  }
  else if ("Save (Add More Material)" == $form_state['values']['op']) {
    drupal_set_message($message);
    unset($_GET['destination']);
    drupal_goto("course-setup/add-materials");
  }
}

/**
 * Implements hook_action_info().
 */
function course_action_info() {
  return array(
    'course_bulk_clone_material' => array(
      'type' => 'node',
      'label' => t('Add Material'),
      'configurable' => FALSE,
    ),
  );
}

function course_bulk_clone_material($biblios, $context = array()) {
  if (!is_array($biblios)) {
    $biblios = array($biblios);
  }
  $docs = array();
  foreach ($biblios as $biblio) {
    $doc = course_clone_material($biblio);
    if (!$doc) {
      drupal_set_message(t('Error adding material for @biblio', array('@biblio' => $biblio->title)), 'warning');
    }
    course_push_unfinished_doc_queue($doc->nid);
  }

}

/**
 * NB: "material" is made up of a biblio entity (canonical info) and a doc entity (course-specific info)
 * Here, we are creating a new doc from an existing biblio, so it's cloning w/ air quotes people.
 */
function course_clone_material($biblio) {
  try {
    $course_nid = course_get_selected_course();
    if (!$course_nid) {
      return false;
    }

    $document = new stdClass();
    $document->type = DOCUMENT_NODE_TYPE;
    $document->language = LANGUAGE_NONE;
    $document->title = $biblio->title;
    $document->field_doc_text[LANGUAGE_NONE][0]['value'] = $biblio->body[LANGUAGE_NONE][0]['value'];
    $document->field_doc_text[LANGUAGE_NONE][0]['format']  = filter_default_format();

    node_object_prepare($document);
    $document->og_group_ref[LANGUAGE_NONE][0]['target_id'] = $course_nid;
    $document->field_bibliographic_entry[LANGUAGE_NONE][0]['target_id'] = $biblio->nid;
    $document->status = NODE_PUBLISHED; # make all documents published
    node_save($document);
    return $document;
  }
  catch (Exception $e) {
    watchdog('course_material_clone', $e, array(), WATCHDOG_ERROR);
    return false;
  }
}

function course_block_info() {
  $blocks = array();
  $blocks['course_finish_material_block'] = array(
    'info' => t('Finish Adding Material'),
    'cache' => DRUPAL_NO_CACHE,
    'status' => TRUE,
    'region' => 'help',
    'visibility' => BLOCK_VISIBILITY_NOTLISTED,
    'pages' => "node/*/edit\nadmin*",
    'theme' => 'lacuna_stories'
  );
  return $blocks;
}


function course_block_view($delta = '') {
  $block = array();
  switch($delta) {
    case 'course_finish_material_block':
      $block['subject'] = NULL;
      $block['content'] = array(
        '#markup' => course_finish_material_block_content()
      );
      break;
  }

  return $block;
}

function course_finish_material_block_content() {
  global $user;
  $key = "Course_Clone_Material_Docs_" . $user->uid;
  $content = '';
  if (isset($_COOKIE['Drupal_visitor_' . $key]) && $docs = $_COOKIE['Drupal_visitor_' . $key]) {
    drupal_add_css(drupal_get_path('module', 'course') . '/course.css');
    $docs = explode(',', $docs);
    foreach($docs as $doc) {
      $url = l("node/$doc/edit","node/$doc/edit");
      $content .= "<div>Please finish adding this Course Material, $url</div>";
    }
  }
  return "<div class='course-finish-adding-material'>" . $content . "</div>";
}

function course_pop_unfinished_doc_queue($nid) {
  global $user;
  $cookie_queue = &drupal_static('course_doc_queue');
  $key = "Course_Clone_Material_Docs_" . $user->uid;
  if (!isset($cookie_queue)) {
    $cookie_queue = $_COOKIE['Drupal_visitor_' . $key];
  }
  $docs = explode(',', $cookie_queue);
  $docs = array_diff($docs, array($nid));
  $cookie_queue = implode(',', $docs);
  user_cookie_save(array($key => $cookie_queue));
}

function course_push_unfinished_doc_queue($nid) {
  global $user;
  $cookie_queue = &drupal_static('course_doc_queue');

  $key = "Course_Clone_Material_Docs_" . $user->uid;
  if (!isset($cookie_queue) && isset($_COOKIE['Drupal_visitor_' . $key])) {
    $cookie_queue = $_COOKIE['Drupal_visitor_' . $key];
  }
  if (!empty($cookie_queue)) {
    $docs = explode(',', $cookie_queue);
  }
  $docs[] = $nid;
  $docs = array_unique($docs);
  $cookie_queue = implode(',', $docs);
  user_cookie_save(array($key => $cookie_queue));
}


/**
 * Implements hook_node_delete(). Delete related taxonomy terms when course deleted.
 */
function course_node_delete($node) {
	if ($node->type == COURSE_NODE_TYPE) {
		$vocabularies = ['units', 'genre_doc', 'genre_response'];
		$query = new EntityFieldQuery();
		$result = $query->entityCondition('entity_type', 'taxonomy_term')
			->entityCondition('bundle', $vocabularies, 'IN')
			->fieldCondition('field_term_course', 'target_id', $node->nid, '=')
			->execute();
		if (isset($result['taxonomy_term'])) {
			foreach(array_keys($result['taxonomy_term']) as $tid) {
				taxonomy_term_delete($tid);
			}
		}
	}
}
